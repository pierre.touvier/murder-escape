From a MURDER to the ESCAPE
===

## Story line 

Concept entre cluedo, murder et escape game. 
Personnages tous plus étranges les uns que les autres.

## Préparation
* Organisateur : 1 à 2
* Nombre de joueur "obligatoire" : 
* Durée : 
* Thème de la soirée : enquête entre fou
* Matèriel requis : 

## Description des personnages

### Psychiatre (p1)

* **Ce qu'il sait :** Personne qui a reçu un message du detective quelques jours apres sa mort présumée/disparition.
* **Ce qu'il ignore :** Tueur de la première victime qui était son psychiatre. Il lui a volé son identité et est devenu lui.

Il ne souhaite pas se dévoiler par peur de devenir suspect.
Personnage shizophrène (ne pas le donner à un des orgas !)

### Extra-lucide (mais surtout amnésique) (p2)

* **Ce qu'il sait :** Personne se souvenant d'information sans savoir comment. Le joueur souhaite élucider le mystère sans que les autres joueurs ne sache qu'il a des infos. Doit apprendre à bluffer et travailler en équipe.
* **Ce qu'il ignore :** Ancien camarade de chambre de la victime. Il est le détective mais suite à ses problèmes psy, il ne s'en souvient pas (blackout).

Note importante :
> ### Le Detective (alias le mort ou le disparu)
> * Pour lui, c'est simple, il est mort, ou en tout cas, il n'est pas là pour en parler. Ce personnage menait l'enquête sur un patient dangeureux d'un hôpital psychiatrique. Mais au moment de rendre son verdicte, il disparait. Heuresement il a dissimulé le résultat de ses recherches dans l'hopital, et à averti son ami sous forme de message codé pour qu'il puisse prendre la relève.

### Un couple (voisin de la victime)

#### Conjoint 1 (p4) :

* **Ce qu'il sait :** Ce joueur est insomniaque, il a vu conjoint 2 se lever dans la nuit et se disputer avec le detective. Il ne parle jamais à ses voisins, il a du mal a sortir de chez lui. Grand fan de séries policières, il est très cultivé. Il fera tout pour protéger son conjoint mais souhaite découvrir la vérité. Il est très facilement angoissé.

#### Conjoint 2 (p5) :

* **Ce qu'il sait :** Ce joueur est somnambule (vive le couple sèrieux!). Il s'est réveillé dans le jardin et n'a pas trouvé sa moitié au lit en retournant se coucher. Il n'est pas du quartier, mais passe beaucoup de temps avec son conjoint qui semble avoir des troubles sociaux. Il s'est toujours réveillé dans des endroits ailleurs que dans son lit depuis enfant.
Il refuse d'en parler à son conjoint pour ne pas l'inquiéter.

### L'inconnu (p3)

* **Ce qu'il sait :** Ami de la première victime (le psychiatre). Il sait aussi que le personnage A n'est pas ce qu'il prétend.

> il va faloir tourner ca différement car ca peut très vite devenir compliqué ou casser tout le suspens
