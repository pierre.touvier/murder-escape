Liste des ressources que l'ont retrouvera ici:
===

## Les différents scenarii :

* [Bonne ambience au pays de Noël](Noel-farceur/Le-Songe-dune-nuit-dhiver.pdf)
* [A l'hopital, on vous soigne !](Hopital-et-medecin/De-Humani-Corporis-Fabrica.pdf)
* [Qui est le plus fou, le fou ? ou le fou qui le suis ?](Enquete-a-l-asile/README.md)
* Fond de frigo:
  * [la murder](fond-de-frigo/Fonds-de-frigo-Murder.pdf)
  * [la teimeline](fond-de-frigo/Fonds-de-frigo-TIMELINE.pdf)
  * [le zip](fond-de-frigo.zip)
* Fond de frigo et plus si affinité:
  * [la murder](Fond-de-frigo_et-plus-si-affinites/document-et-contenu/Guide-de-la-murder.pdf)
  * [tous les outils](Fond-de-frigo_et-plus-si-affinites/)
  * [le zip](Fond-de-frigo_et-plus-si-affinites.zip)
* Meurtre et caviar (nouvel an):
  * [la murder](Meurtre-et-caviar/Murder_Assouvir.docx)
  * [tous les outils](Meurtre-et-caviar/)
  * [le zip](Meurtre-et-caviar.zip)